package wrapper;
import java.util.ArrayList;


public class ArrayStringhe {
	public static void main(String[] args) {
		
		/*
		String moto1,moto2,moto3;
		
		moto1="Honda";
		moto2="Ducati";
		moto3="Yamaha";
		*/
		String [] moto = new String[10];
		moto[1] = "Honda";
		moto[2]  = "Ducati";
		moto[3]  = "Yamaha";
		
		String[] auto = {"Fiat","Ferrari" , "Lamborghini"};
		
		System.out.println(auto.length);
		
		ArrayList <String> veicoli = new ArrayList <>();
		veicoli.add("Bianchi");
		veicoli.add(moto[1]);
		veicoli.add(auto[0]);
		veicoli.add("Monopattino elettrico");
		veicoli.add("Ferrari");
		
		for (int i = 0; i < auto.length; i++) {
			System.out.println(auto[i]);
		}
		System.out.println("----");
		for (String elemento : veicoli) {
			System.out.println(elemento);
		}
		
	}
}
