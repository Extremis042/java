package wrapper;
import java.util.Scanner;

public class ProvaWrapper {
	public static void main(String args[]) {
		try {
			Scanner scan = new Scanner (System.in);
			int input1,input2;
			
			System.out.println("Inserisci un numero intero da 1 a 10.");
			input1= Integer.parseInt(scan.nextLine());
			System.out.println(input1);
			
			System.out.println("Inserisci un numero intero da 1 a 10.");
			input2= Integer.parseInt(scan.nextLine());
			
			System.out.println(input1+input2);
			
			scan.close();
			
		}catch(Exception e) {
			System.out.println("Errore :"+e);
		}
		
	}
}
