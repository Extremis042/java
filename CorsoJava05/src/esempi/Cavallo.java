package esempi;

public class Cavallo {
	private String nome;
	private int velocitaBase;
	private int metriPercorsi;
	
	//COSTRUTTORE
	public Cavallo(String nome, int vb) {
		this.nome=nome;
		this.velocitaBase=vb;
	}
	
	//GETTERS E SETTERS
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public int corri(int stimolo) {
		int distanza=0,fattoreCavallo;
		
		fattoreCavallo=(int)(Math.random()*10);
		distanza=stimolo*velocitaBase*fattoreCavallo;
		this.metriPercorsi=metriPercorsi+distanza;
		return distanza;
	}
	
	@Override
	public String toString() {
		String s="\n\nDATI DEL CAVALLO: \n"+" NOME: "+nome
				+"\n VELOCITA : "+velocitaBase+"\n METRI PERCORSI: "+metriPercorsi+"\n\n";
		
		return s;
	}
	
}
