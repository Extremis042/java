package esempi;
import esempi.Login;

public class Laboratorio {

	public static void main(String args[]) {
		
		System.out.println("Cavalli magazine!");
		System.out.println("Effettua il login");
		if(args.length>1){
			//variabile locale
			Login.verificaLogin(args);
		}else {
			System.out.println("Inserire nome utente e password.");
		}
		
		Cavallo bianco = new Cavallo("pippo",15);
		Cavallo nero = new Cavallo("paperino",25);
		Cavallo azzurro = new Cavallo("pluto",35);
		Cavallo lezzo = azzurro; //puntatore
		
		bianco.corri(15);
		nero.corri(18);
		azzurro.corri(20);
		lezzo.corri(22);
		
		System.out.println(bianco);
		System.out.println(nero);
		System.out.println(azzurro);
		System.out.println(lezzo);
		
	}
}
