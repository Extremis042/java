package esercizio04;
import java.util.Scanner;


public class ScannerLogin {
	public static void main(String args[]){
		String risposta;
		
		System.out.println("Per accedere devi effettuare il login");
		risposta=login();
		System.out.println("User : " + risposta);
	}
	
	private static String login() {
		String nome,cognome,risposta=null;
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Inserisci le credenziali");
		
		System.out.println("\t Inserisci il tuo nome:");
		nome=scan.nextLine();
		
		System.out.println("\t Inserisci il tuo cognome:");
		cognome=scan.nextLine();
		
		if(nome.equals("nome") && cognome.equals("cognome")){
			
			risposta="connected";
		}else {
			risposta="not connected";
		}
		return risposta;
	}
}
