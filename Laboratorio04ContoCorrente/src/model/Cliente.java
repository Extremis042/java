package model;

/**
 * La classe cliente possiede:
 * @param nome : il nome del cliente
 * @param conti : i conti in suo possesso (non passabili all'esterno).
 * 				  i conti sono finiti (MAX 4)
 * */
public class Cliente {
	private String nome;
	private ContoCorrente[] conti;
	
	public Cliente(String nome){
		this.nome=nome;
		conti = new ContoCorrente[4];
	}
	
	
	public void collegaContoPrincipale(ContoCorrente conto) {
		this.conti[0]=conto;
	}
	public void collegaContoSecondario(ContoCorrente conto,int i) {
		if(i > 0 && i<conti.length) {
			this.conti[i]=conto;
		}
	}
	
	/**
	 * Il metodo stampaPatrimonio richiama il vettore di conti in possesso
	 * del cliente, per poi sommare il saldo contenuto nei suoi conti (contenuti
	 * nella funzione getSaldo) 
	 * se il valore contenuto nel vettore esiste (!= null)
	 * */
	public String stampaPatrimonio() {
		double somma=0d;
		
		for(int i=0;i<conti.length;i++) {
			if(conti[i]!=null)
			somma=somma+conti[i].getSaldo();
		}
		
		return "Il patrimonio del cliente " + nome+ " � di : "+somma+"\n";
	}
	
}
