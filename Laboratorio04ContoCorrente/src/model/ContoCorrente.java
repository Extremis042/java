package model;

public class ContoCorrente {
	private double saldo;
	private int numeroConto;
	private final double TASSO_INTERESSE = 5;
	
	/**
	 * ContoCorrente - Costruttore del conto corrente
	 * COSTRUTTORE :
	 * Nella creazione il saldo � a 0, il numero di conto � fornito o generato da altri
	 * @param numeroConto: numero di conto proveniente dall'esterno
	*/
	public ContoCorrente(int numeroConto){
		this.saldo=0d;
		this.numeroConto=numeroConto;
	}
	
	
	public double getSaldo() {
		return this.saldo;
	}

	public void versamento(double ammontare) {
		saldo=saldo+ammontare;
	}
	
	public double prelievo(double ammontare) {
		if(saldo>=ammontare) {
			saldo=saldo-ammontare;
		}else {
			ammontare=0d;
		}
		return ammontare;
	}


	@Override
	public String toString() {
		return "Il conto corrente : "+numeroConto + "\n"
				+"Ha saldo di EUR: "+saldo+"\n\n";
	}
	
	
	
}
