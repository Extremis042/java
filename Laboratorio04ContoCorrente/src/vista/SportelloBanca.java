package vista;
import controller.ContoController;
import model.ContoCorrente;
import model.Cliente;

public class SportelloBanca {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ContoController control= new ContoController();
		ContoCorrente conto1= control.creaConto();
		ContoCorrente conto2= control.creaConto();
		Cliente cliente1= new Cliente("NomeCliente");
		Cliente cliente2= new Cliente("Cliente2");
		
		/*Collego i conti creati in precedenza al cliente1*/
		cliente1.collegaContoPrincipale(conto1);
		cliente1.collegaContoSecondario(conto2, 1);
		cliente2.collegaContoSecondario(conto2, 1);
		
		//stampo i dati prima del versamento 
		System.out.println(conto1.getSaldo());
		System.out.println(conto2.getSaldo());
		
		
		//effettuo un versamento e un prelievo nei conti
		conto1.versamento(500);
		conto2.versamento(100);
		conto2.prelievo(10);
		
		//stampo il saldo dei conti singolarmente
		System.out.println("Il saldo del conto 1 � di: "+conto1.getSaldo());
		System.out.println("Il saldo del conto 2 � di: "+conto2.getSaldo());
		
		//stampo il patrimonio di entrambi i clienti
		System.out.println(cliente1.stampaPatrimonio());
		System.out.println(cliente2.stampaPatrimonio());
	}

}
