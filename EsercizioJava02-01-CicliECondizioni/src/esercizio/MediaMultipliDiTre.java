package esercizio;
import java.util.Scanner;


public class MediaMultipliDiTre {
	public static void main(String args[]) {
		//Inserimento del try - catch in modo da impedire eventuali errori da inserimento
		try {
			/* Dichiarazione delle variabili
			 * scan : Indica l'oggetto che si occupa di leggere l'input dell'utente
			 * n : Indica la quantit� di numeri inseriti dall'utente
			 * i : Indica l'indice che scorrer� nel ciclo
			 * num : Indica il numero inserito dall'utente
			 * c : Indica un contatore che conta ogni numero divisibile per 3
			 * media : indica la media dei numeri divisibili per 3
			 *  */
			Scanner scan= new Scanner(System.in);
			int n,i=0,num,c=0;
			double media=0;
			
			
			System.out.println("Inserire la quantit� di numeri di cui si sta parlando");
			n=Integer.parseInt(scan.nextLine());
			while(i<n) {
				System.out.println("Inserire un numero da leggere:");
				num=Integer.parseInt(scan.nextLine());
				if(num%3==0){
					media=media+num;
					c++;
				}
				i++;
			}
			if(media!=0) {
				media=media/c;
			}
			System.out.println("La media dei numeri divisibili per 3 � di :"+media);
			
		}catch(Exception e) {
			System.out.println("Errore: "+e);
		}
	}
}
