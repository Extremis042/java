package controller;
import model.*;
import java.util.ArrayList;

public class Contenitore {
	ArrayList <Punto> punti = new ArrayList <Punto>();
	ArrayList<Segmento> segmenti =  new ArrayList <>();
	ArrayList<Triangolo> triangoli =  new ArrayList <>();
	
	/**
	 * Inserisce alcuni punti dentro il contenitore
	 * */
	public void caricaPunti() {
		Punto a= new Punto(1,1) ,b= new Punto(5,2) , c= new Punto(5,2);
		
		punti.add(a);
		punti.add(b);
		punti.add(c);
		
		/**
		 * Crea un nuovo segmento creandolo con new
		 * */
		segmenti.add(new Segmento(a , b ) );
		segmenti.add(new Segmento(a , c) );
		segmenti.add(new Segmento(b , c) );
		
		triangoli.add(new Triangolo(a,b,c));
	}

	/**
	 * @return the punti
	 */
	public ArrayList<Punto> getPunti() {
		return punti;
	}
	public ArrayList<Segmento> getSegmenti() {
		return segmenti;
	}
	public ArrayList<Triangolo> getTriangoli() {
		return triangoli;
	}
	
	
}
