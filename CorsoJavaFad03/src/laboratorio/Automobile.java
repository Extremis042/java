package laboratorio;

public class Automobile {
	private int numeroRuote =4 ;
	private String marca="Ferrari";
	private String modello="F500";
	private String colore="rosso";
	
	
	
	
	
	/**
	 * @return the numeroRuote
	 */
	public int getNumeroRuote() {
		return numeroRuote;
	}
	/**
	 * @param numeroRuote the numeroRuote to set
	 */
	public void setNumeroRuote(int numeroRuote) {
		this.numeroRuote = numeroRuote;
	}
	/**
	 * @return the marca
	 */
	public String getMarca() {
		return marca;
	}
	/**
	 * @param marca the marca to set
	 */
	public void setMarca(String marca) {
		this.marca = marca;
	}
	/**
	 * @return the modello
	 */
	public String getModello() {
		return modello;
	}
	/**
	 * @param modello the modello to set
	 */
	public void setModello(String modello) {
		this.modello = modello;
	}
	/**
	 * @return the colore
	 */
	public String getColore() {
		return colore;
	}
	/**
	 * @param colore the colore to set
	 */
	public void setColore(String colore) {
		this.colore = colore;
	}
	
	public void velocitaMax() {
		System.out.println("La velocit� max � di "+600+" km");
	}
	
	public void velocitaMax(String s) {
		System.out.println("La velocit� � di: "+800 +"km");
	}
	public void velocitaMax(int i) {
		System.out.println("La velocit� � di: "+800 +"km");
	}
	
	@Override
	public String toString() {
		return "Automobile [numeroRuote=" + numeroRuote + ", marca=" + marca + ", modello=" + modello + ", colore="
				+ colore + "]";
	}
	
	
}
