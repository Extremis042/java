package laboratorio;
import java.util.ArrayList;


public class ProvaObject {

	public static void main(String[] args) {
		Automobile auto= new Automobile();
		auto.setModello("F650");
		System.out.println(auto);
		
		
		Automobile fuoriStrada= new FuoriStrada();
		FuoriStrada fuori= new FuoriStrada();
		fuori.setMarca("BMW");
		fuori.velocitaMax();
		
		ArrayList<Automobile> garage = new ArrayList<Automobile>();
		garage.add(auto);
		garage.add(fuori);
		
		System.out.println("Auto nel garage: " +garage.size());
		
		garage.remove(auto);
		System.out.println("Auto nel garage: " +garage.size());
		
		for(Automobile aut : garage) {
			System.out.println(aut);
		}
		
	}

	
}
