package view;
import controller.*;
import model.*;

public class ProvaGeometria {
	public static void main(String[] args) {
		Contenitore cont= new Contenitore();
		cont.caricaPunti();
		
		for(Punto p: cont.getPunti()) {
			System.out.println(p);
		}
		
		for(Segmento s: cont.getSegmenti()) {
			System.out.println(s);
		}
		for (Triangolo t: cont.getTriangoli()) {
			System.out.println(t);
		}
	}
}
