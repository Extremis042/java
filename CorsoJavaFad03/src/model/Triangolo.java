package model;

public class Triangolo {
	private Punto puntoA,puntoB,puntoC;
	private Segmento segmntAB,segmntBC,segmntCA;
	private double superfice;
	private double perimetro;
	
	public Triangolo(Punto puntoA,Punto puntoB,Punto puntoC){
		this.puntoA=puntoA;
		this.puntoB=puntoB;
		this.puntoC=puntoC;
	}
	
}
