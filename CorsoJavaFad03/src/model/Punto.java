package model;

public class Punto {
	private int coordinateX, coordinateY;
	
	/**
	 * Creazione di un punto sull'origine
	 * 
	 * */
	public Punto() {
		coordinateX=0;
		coordinateY=0;
	}

	
	
	/**
	 * Creazione di un punto avendo solo la X
	 * @param coordinateX Le coordinate X
	 */
	public Punto(int coordinateX) {
		super();
		this.coordinateX = coordinateX;
	}



	/**
	 * Creazione di un punto su due punti dati
	 * @param coordinateX Le coordinate X
	 * @param coordinateY Le coordinate Y
	 * */
	public Punto(int coordinateX, int coordinateY) {
		super();
		this.coordinateX = coordinateX;
		this.coordinateY = coordinateY;
	}



	/**
	 * @return the coordinateX
	 */
	public int getCoordinateX() {
		return coordinateX;
	}



	/**
	 * @param coordinateX the coordinateX to set
	 */
	public void setCoordinateX(int coordinateX) {
		this.coordinateX = coordinateX;
	}



	/**
	 * @return the coordinateY
	 */
	public int getCoordinateY() {
		return coordinateY;
	}



	/**
	 * @param coordinateY the coordinateY to set
	 */
	public void setCoordinateY(int coordinateY) {
		this.coordinateY = coordinateY;
	}



	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Punto [coordinateX=");
		builder.append(coordinateX);
		builder.append(", coordinateY=");
		builder.append(coordinateY);
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
