package model;

public class Segmento {
	private Punto puntoA,puntoB;
	
	public Segmento() {
		puntoA= new Punto(1,1);
		puntoB= new Punto(5,2);
	}
	
	public Segmento(Punto puntoA, Punto puntoB) {
		this.puntoA=puntoA;
		this.puntoB=puntoB;
		
	}

	/**
	 * @return the puntoA
	 */
	public Punto getPuntoA() {
		return puntoA;
	}

	/**
	 * @param puntoA the puntoA to set
	 */
	public void setPuntoA(Punto puntoA) {
		this.puntoA = puntoA;
	}

	/**
	 * @return the puntoB
	 */
	public Punto getPuntoB() {
		return puntoB;
	}

	/**
	 * @param puntoB the puntoB to set
	 */
	public void setPuntoB(Punto puntoB) {
		this.puntoB = puntoB;
	}
	
	public double calcolaLunghezza() {
		return 0d;
	}
}
