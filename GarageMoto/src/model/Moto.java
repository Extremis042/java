package model;

public class Moto {
	/*Dichiarazione Attributi*/
	private String marca,modello,categoria;
	private int cilindrata;
	private String immagine;
	
	
	/**
	 * COSTRUTTORE
	 * @param marca La marca della moto passata dall'esterno
	 * @param modello Il modello della moto passata dall'esterno
	 * @param categoria La categoria della moto passata dall'esterno
	 * @param cilindrata La cilindrata della moto passata dall'esterno
	 * */
	public Moto(String marca, String modello, String categoria, int cilindrata) {
		this.marca = marca;
		this.modello = modello;
		this.categoria = categoria;
		this.cilindrata = cilindrata;
	}

	/* GETTERS e SETTERS */
	
	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModello() {
		return modello;
	}

	public void setModello(String modello) {
		this.modello = modello;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public int getCilindrata() {
		return cilindrata;
	}

	public void setCilindrata(int cilindrata) {
		this.cilindrata = cilindrata;
	}

	public String getImmagine() {
		return immagine;
	}

	public void setImmagine(String immagine) {
		this.immagine = immagine;
	}
	
	
	
	
}
