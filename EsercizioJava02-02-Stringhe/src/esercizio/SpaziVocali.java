package esercizio;
import java.util.Scanner;

public class SpaziVocali {
	public static void main(String args[]) {
		try {
			/* Dichiarazione di variabili */
			Scanner scan = new Scanner(System.in);
			String frase;
			int c=0;
			
			System.out.println("Inserisci una frase");
			frase=scan.nextLine();
			for(int i=0;i<frase.length(); i++) {
				if(frase.charAt(i)==' ' ) {
					char car = frase.charAt(i+1);
					if (car== 'a' || car=='e' || car== 'i' || car=='o' || car=='u' ) {
						c++;	
					}
					
				}
			}
			System.out.println(c);
			
		}catch(Exception e) {
			System.out.println("Errore : "+e);
		}
	}
}
